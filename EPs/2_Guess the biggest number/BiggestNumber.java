import java.util.Random;

public class BiggestNumber {

	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);		//First number chosen by alice
		int b = Integer.parseInt(args[1]);		//Second number chosen by alice
		int nGames = Integer.parseInt(args[2]);	//Number of games played

		boolean answer = a > b;	//True if the first card is higher than the second
		boolean guess;			//Our guess as to whether a > b. May change with each game
		int score = 0;				//Total number of games won

		for (int i = 0; i < nGames; i++) {
			
			if (a < (Math.random() * 100)) {	//
				guess = false;
			} else {				
				guess = true;		//	
			}

			if (guess == answer) score++;	//If we guessed right, increment our score
		}

		System.out.println(score + " [" + (double)score/nGames + "/" + ((double)(score/nGames) - 0.5) + "]");
	}    
}




























