/*
 * Este programa implementa a estratégia óbvia, que dá 50% de 
 * chance de vitória para Bob.
 * 
 * Uso: java BiggestNumberDumb a b T
 * 
 * onde a e b são os inteiros escolhidos por Alice (no intervalo 0..100)
 * e T é o número de vezes que o jogo é simulado.
 *
 * Exemplos de execução:
 * 
 * $ java BiggestNumberDumb 35 70 1000
 * 480 [0.48/-0.020000000000000018]
 * $ java BiggestNumberDumb 35 70 10000
 * 5020 [0.502/0.0020000000000000018]
 * $ java BiggestNumberDumb 35 70 100000
 * 49989 [0.49989/-1.0999999999999899E-4]
 * $ java BiggestNumberDumb 35 70 1000000
 * 499894 [0.499894/-1.0599999999999499E-4]
 * 
 * Com uma estratégia melhor (implementada no programa 
 * BiggestNumber.java), Bob consegue uma boa margem de vitória: 
 * 
 * $ java BiggestNumber 35 70 1000
 * 702 [0.702/0.20199999999999996]
 * $ java BiggestNumber 35 70 10000
 * 6761 [0.6761/0.17610000000000003]
 * $ java BiggestNumber 35 70 100000
 * 67699 [0.67699/0.17698999999999998]
 * $ java BiggestNumber 35 70 1000000
 * 674776 [0.674776/0.17477600000000004]
 *
 * Seu objetivo é encontrar uma tal estratégia para Bob.
 */

public class BiggestNumberDumb {

    public static void main(String[] args) {
	int a = Integer.parseInt(args[0]);
	int b = Integer.parseInt(args[1]);
	int T = Integer.parseInt(args[2]);

	int bigger;
	if (a < b) bigger = 1;
	else bigger = 0;

	int right = 0;
	for (int i = 0; i < T; i++) {
	    // Estratégia ingênua de Bob (que não é boa)
	    int choice;
	    if (Math.random() < .5) choice = 0;
	    else choice = 1;

	    // Escolha correta?
	    if (choice == bigger) right++;
	}
	// Estatística
	double f = (double)right/T;
	System.out.println(right + " [" + f + "/" + (f - .5) + "]");
    }
    
}
