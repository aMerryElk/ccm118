public class VotingMachines {
	public static void main(String[] args) {

		int		voters =	Integer.parseInt(args[0]);		//Total number of participants
		double	votersA =	Double.parseDouble(args[1]);	//Fraction of A voters
		double	error =		Double.parseDouble(args[2]);	//Chance of registering error
		int		sims =		Integer.parseInt(args[3]);		//Number of simulations


		if (isReliable(voters, votersA, error, sims, 0.03)) {
			System.out.println("Reliable");
		} else {
			System.out.println("Not reliable");
		}	
	}

	/*================================================================================
	||	This function is supposed to simulate an election of two parties, given a	||
	||	chance of error in computation for every vote. However, it aparently gives	||
	||	me different results from the ones laid in the example.						||
	||																				||
	||	I've spent way mre time than I should have trying to figure out what was	||
	||	the logic behind the examples' results, but I still haven't done it.		||
	||	Since this is the case, I'm just going to carefully document every step		||
	||	in hopes that it makes sense to you as it does to me.						||
	||																				||
	||	Basically, I can write a function that seems reasonable and gives me any	||
	||	of the results from the listed examples, but not a function that gives me	||
	||	all of them.																||
	================================================================================*/
	public static int numberOfAVotes(int voters, double votersA, double error) {

		//Starts with the correct proportion of votes
		int votesForA = (int)(voters*votersA);

		while (voters > 0) { //If there are still voters left, continue
			if (error > Math.random()) {
			//Incorrectly registered
				
				//Whose vote was registered incorrectly?
				if (Math.random() < votersA) {
				//It was an A-voter's
					votesForA--;

				} else {
				//It was a B-voter's
					votesForA++;
				}
			}
			voters--; //One less voter to go
		}

		return votesForA;
	}

	//------------------------------------------------------------------------------------
	public static double failureRatio(int voters, double votersA, double error, int sims) {

		int failures = 0;

		for (int i = 0; i < sims; i++) {
			int result = numberOfAVotes(voters, votersA, error);
			
			if (result < voters/2) {
				failures++;
			}
		}

		System.out.println("Wrong results/trials: " + failures + "/" + sims);

		return (double)failures/sims;
	}

	//----------------------------------------------------------------------------------------------------
	public static boolean isReliable(int voters, double votersA, double error, int sims, double tolerance) {

		if (failureRatio(voters, votersA, error, sims) > tolerance) {
			return false;
		}
		return true;
	}
}