public class PVotingMachinesDeluxe {
	public static void main(String[] args) {

		int		voters =	Integer.parseInt(args[0]);		//Total number of participants
		double	votersA =	Double.parseDouble(args[1]);	//Fraction of A voters
		int		sims =		Integer.parseInt(args[2]);		//Number of simulations

		//Optional argument denoting number of splits done when performing binary search
		int searchDepth = 10;
		if (args.length > 3) {
			searchDepth = Integer.parseInt(args[3]);	
		}

		double maxErrorAllowed = 0; //Max known error alowed. (Starts at 0 because 0 error is alwaya allowed)

		boolean[] map = new boolean[searchDepth]; //0(F) if lower half, 1(T) if upper half	//DEBUG

		double max = 0.25; //Splits at the mid point
		double min = 0.00;
		for (int i = 0; i < searchDepth; i++) {

			double newDiff = (max-min)/2;
				
			if (isReliable(voters, votersA, max, sims, 0.03)) {
			//Max error is above the middle point
				maxErrorAllowed = max; //Update max known error alowed
				map[i] = true;
				min = max;
				max = max + newDiff;
			} else {
			//Max error is below the middle point
				map[i] = false;
				max = max - newDiff;
			}
		}

//		System.out.println(arrayToString_format_bool(map));		//DEBUG

		System.out.println("Tolerable error: " + maxErrorAllowed);

	}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	/*================================================================================
	||	This function is supposed to simulate an election of two parties, given a	||
	||	chance of error in computation for every vote. However, it aparently gives	||
	||	me different results from the ones laid in the example.						||
	||																				||
	||	I've spent way mre time than I should have trying to figure out what was	||
	||	the logic behind the examples' results, but I still haven't done it.		||
	||	Since this is the case, I'm just going to carefully document every step		||
	||	in hopes that it makes sense to you as it does to me.						||
	||																				||
	||	Basically, I can write a function that seems reasonable and gives me any	||
	||	of the results from the listed examples, but not a function that gives me	||
	||	all of them.																||
	================================================================================*/
	public static int numberOfAVotes(int voters, double votersA, double error) {

		//Starts with the correct proportion of votes
		int votesForA = (int)(voters*votersA);

		while (voters > 0) { //If there are still voters left, continue
			if (error > Math.random()) {
			//Incorrectly registered
				
				//Whose vote was registered incorrectly?
				if (Math.random() < votersA) {
				//It was an A-voter's
					votesForA--;

				} else {
				//It was a B-voter's
					votesForA++;
				}
			}
			voters--; //One less voter to go
		}

		return votesForA;
	}

	//------------------------------------------------------------------------------------
	public static double failureRatio(int voters, double votersA, double error, int sims) {

		int failures = 0;

		for (int i = 0; i < sims; i++) {
			int result = numberOfAVotes(voters, votersA, error);
			
			if (result < voters/2) {
				failures++;
			}
		}

		return (double)failures/sims;
	}

	//----------------------------------------------------------------------------------------------------
	public static boolean isReliable(int voters, double votersA, double error, int sims, double tolerance) {

		if (failureRatio(voters, votersA, error, sims) > tolerance) {
			return false;
		}
		return true;
	}


	//DEBUG
	public static String arrayToString_format_bool(boolean[] arr){

		String result = "[";

		for (int i = 0; i < arr.length; i++) {
			int element = arr[i] ? 1 : 0;
			if (i != arr.length-1) {
				result = result + element + ", ";
			} else {
				result = result + element + "]";
			}
		}
		return result;
	}
}