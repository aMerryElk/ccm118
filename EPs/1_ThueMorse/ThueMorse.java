public class ThueMorse {
	public static void main(String[] args) {

		String a1 = "0";	// The first and second "fundamental" characters
		String a2 = "1";	//
		System.out.println(a1 + a2);

		String b1 = a1 + a2;
		String b2 = a2 + a1;
		System.out.println(b1 + b2);

		String c1 = b1 + b2;
		String c2 = b2 + b1;
		System.out.println(c1 + c2);

		String d1 = c1 + c2;
		String d2 = c2 + c1;
		System.out.println(d1 + d2);

		String e1 = d1 + d2;
		String e2 = d2 + d1;
		System.out.println(e1 + e2);

		String f1 = e1 + e2;
		String f2 = e2 + e1;
		System.out.println(f1 + f2);

		// Each line can be divided into four parts:
		// The 1st 1/4 of any line is the first half (A) of its preceding line
		// The 2nd 1/4 of any line is the second half (B) of its preceding line
		// The 3nd 1/4 of any line is the second half (B) of its preceding line
		// The 4nd 1/4 of any line is the first half (A) of its preceding line

		// Such that it forms the pattern, in reference to its preceding line:
		// A B B A

		// [*you're the Dancing Queeeen*]
	}
}