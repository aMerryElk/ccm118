public class CompareDice {

	public static void main(String[] args) {

		int a[] = new int[Integer.parseInt(args[0])]; 				// First argument determines the number of sides on A
		int b[] = new int[Integer.parseInt(args[a.length + 1])];	// The argument after all the sides in A is the number of sides on B
		int nGames = Integer.parseInt(args[args.length - 1]);		// How many games will be played

		// Creates the array that represents the dice using the buildDie() function
		a = buildDie(0, args);
		b = buildDie(Integer.parseInt(args[0])+1, args);

		// Outputs all the possible sides of each die
		System.out.print("A: "); for (int i = 0; i < a.length; i++) {System.out.print(a[i] + " ");}
		System.out.println();
		System.out.print("B: "); for (int i = 0; i < b.length; i++) {System.out.print(b[i] + " ");}
		System.out.println();

		int aWins = 0;
		int bWins = 0;
		for (int i = 0; i < nGames; i++) {

			int aSide = (int) (Math.random() * a.length);	// Cast die A
			int bSide = (int) (Math.random() * b.length);	// Cast die B
			
			if (a[aSide] > b[bSide]) {			// A is larger
				aWins ++;
			} else if (a[aSide] < b[bSide]) {	// B is larger
				bWins ++;
			}
		}

		System.out.println("Vitórias de A: " + aWins);
		System.out.println("Vitórias de B: " + bWins);
		System.out.println("Probability that A wins: " + ((double) aWins)/(aWins + bWins));
	}


	public static int[] buildDie(int dieIndex, String[] args) {	// Organises the arguments into a die
		// dieIndex -	index of the element within args[] that determines the number of sides on the die
		// args -		the array of strings received as arguments for the program

		int[] die = new int[Integer.parseInt(args[dieIndex])];

		for (int i = 0; i < die.length; i++) {
			die[i] = Integer.parseInt(args[dieIndex + i + 1]);		
		}

		return die;	// *Cue heavy metal riff*
	} 
}

/*
==== MELHOR ESTRATÉGIA PARA ALICE =========================

	D_1 = (2, 6, 7)
	D_2 = (1, 5, 9)
	D_3 = (3, 4, 8)

	Dado que temos 3 dados, e que podemos assumir que uma partida entre
	dois dados identicos é de 50%, podemos testar apenas as 3 possibilidades:

	______________________________________
	1 vs 2:
			A: 2 6 7 
			B: 1 5 9 
			Vitórias de A: 55710
			Vitórias de B: 44290
			Probability that A wins: 0.5571
	______________________________________
	1 vs 3:
			A: 2 6 7 
			B: 3 4 8 
			Vitórias de A: 44463
			Vitórias de B: 55537
			Probability that A wins: 0.44463
	______________________________________
	2 vs 3:
			A: 1 5 9 
			B: 3 4 8 
			Vitórias de A: 55628
			Vitórias de B: 44372
			Probability that A wins: 0.55628
	______________________________________

	
	Portanto, Alice deveria escolher em segundo lugar. Sendo ótimas as
	seguintes estratégias em função da escolha de Bob:

	D_1: Alice deve escolher D_3
	D_2: Alice deve escolher D_1
	D_3: Alice deve escolher D_2

===========================================================
*/