public class TreeDeluxe {
	public static void main(String[] args) {

		int depth =				Integer.parseInt(args[0]);
		double rootAngle =		Math.toRadians(Integer.parseInt(args[1]));
		double branchRadius =	Double.parseDouble(args[2]);
		double bendAngle =		Math.toRadians(Integer.parseInt(args[3]));
		double branchAngle =	Math.toRadians(Integer.parseInt(args[4]));
		double branchRatio =	Double.parseDouble(args[5]);

		String fileName =	"tree-" +
							args[0] + "-" +
							args[1] + "-" +
							args[2] + "-" +
							args[3] + "-" +
							args[4] + "-" +
							args[5] + ".png";

		StdDraw.enableDoubleBuffering();
		StdDraw.setCanvasSize(700, 700);
		tree(depth, 0.5, 0, rootAngle, branchRadius, bendAngle, branchAngle, branchRatio);
		StdDraw.show();
		StdDraw.save(fileName);
	}

	public static void tree(int depth, double rootX, double rootY, double a, double branchRadius, double bendAngle, double branchAngle, double branchRatio) {
		
		double cx = rootX + Math.cos(a) * branchRadius;
		double cy = rootY + Math.sin(a) * branchRadius;
		StdDraw.setPenRadius(0.001 * Math.pow(depth, 1.2));
		StdDraw.line(rootX, rootY, cx, cy);
		if (depth == 0) return;

		tree(depth-1, cx, cy, a + bendAngle - branchAngle, branchRadius * branchRatio,			bendAngle, branchAngle, branchRatio);
		tree(depth-1, cx, cy, a + bendAngle + branchAngle, branchRadius * branchRatio,			bendAngle, branchAngle, branchRatio);
		tree(depth-1, cx, cy, a + bendAngle,               branchRadius * (1 - branchRatio),	bendAngle, branchAngle, branchRatio);
	}

}
