/******************************************************************************
 *  Compilation:  javac LFSR.java
 *  Execution:    java LFSR N
 *  
 *  Simulate a LFSR for N steps and print results.
 *
 *  % java LFSR2 40
 *  1100100100111101101110010110101110011000
 *
 ******************************************************************************/

public class LFSR2 {

    public static void main(String[] args) { 
        // initial fill
        boolean[] a = { false, true, false, false, false,
                        false, true, false, true, true, false
                      };
        int T = Integer.parseInt(args[0]);    // number of steps
        int N = a.length;                     // length of register
        int TAP = 8;                          // tap position


        // Simulate operation of shift register.
        for (int t = 0; t < T; t++) {

           // Simulate one shift-register step.
           boolean next = (a[N-1] ^ a[TAP]);  // Compute next bit.

           for (int i = N-1; i > 0; i--)
              a[i] = a[i-1];                  // Shift one position.

           a[0] = next;                       // Put next bit on right end.

           if (next) System.out.print("1");
           else      System.out.print("0");    
         }
        System.out.println();    
    }
}


