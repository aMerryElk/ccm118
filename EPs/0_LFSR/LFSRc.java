public class LFSRc {

	public static void main (String[] args) {

		int[] seed = {0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0};	//Seed for generating the sequence

		int tap0 = 11;								//Not an argument. Determines first tap
		int tap1 = Integer.parseInt(args[0]);		//1st arg determines second tap. If 11, output will be all 0s
		int bits = Integer.parseInt(args[1]);		//2nd arg is the number of bits to output
		
		for (int i = 0; i < bits;) {				//Loop this code for each bit in the sequence of bit to output. It runs for as long as i, which starts at 0 is lesser than the total number of bits that we're supposed to output
			seed[0] = (seed[tap0] ^ seed[tap1]);	//XOR the bit at tap0(hardcoded) with the bit at tap1(defined by the 1st argument)
			System.out.print(seed[0]);				//Outputs the bit resultant from the XOR operation into the console

			for (int ii = 11; ii > 0;) {			//This loop shifts the bit register 11 bits to the right. It runs only while ii, which starts at 11, remains greater than 0
				seed[ii] = seed[ii - 1];			// Each bit gets overwritten with the one on its left
				ii = ii - 1;						//Decrease iterator for next loop
			}

			i = i + 1;								//Increment iterator for next loop
		}
		System.out.println();						//Output a new empty line before exiting just for neatness
	}
}