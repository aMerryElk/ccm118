public class SelfAvoidingWalkDeluxe { 

	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);

		StdDraw.setXscale(0, n);
		StdDraw.setYscale(0, n);

		// draw many self-avoiding random walks
		while (true) {
		   boolean[][] visited = new boolean[n][n];

			// starting position
			int x = n / 2;
			int y = n / 2;
			visited[x][y] = true;

			StdDraw.clear();
			StdDraw.setPenColor(StdDraw.RED);
			StdDraw.filledSquare(x + 0.5, x + 0.5, 0.45);
			StdDraw.show();
			StdDraw.pause(25);
			StdDraw.setPenColor(StdDraw.BLACK);
			StdDraw.setPenRadius(.7/n);								//

			// make a random move as long as particle is not boxed in
			while (!visited[x-1][y] || !visited[x+1][y] || !visited[x][y-1] || !visited[x][y+1]) {

				// try until you find an available move
				while (true) {
					double r = StdRandom.uniform();
					if (r < 0.25 && !visited[x-1][y]) {				// East
						StdDraw.line(x+0.5, y+0.5, x+0.5-1, y+0.5);	//
						x--;
						break;
					}
					else if (r < 0.50 && !visited[x+1][y]) {		// West
						StdDraw.line(x+0.5, y+0.5, x+0.5, y+0.5);	//
						x++;
						break;
					}
					else if (r < 0.75 && !visited[x][y-1]) {		// South
						StdDraw.line(x+0.5, y+0.5, x+0.5, y+0.5-1);	//
						y--;
						break;
					}
					else if (r < 1.00 && !visited[x][y+1]) {		// North
						StdDraw.line(x+0.5, y+0.5, x+0.5, y+0.5+1);	//
						y++;
						break;
					}
				}
				visited[x][y] = true;

				// draw
				StdDraw.pause(25);

				if (x <= 0 || x >= n-1 || y <= 0 || y >= n-1) break;    // hit outside boundary
			}
			StdDraw.pause(1000);
		}
	}
}

