public class LiaSurpresa {
	public static void main(String[] args) {
		
		int numAmigos = Integer.parseInt(args[0]);
		int numSimulacoes = Integer.parseInt(args[1]);

		int[] amigos = new int[numAmigos];
		for (int i = 0; i < numAmigos; i++) {
			amigos[i] = i;
		}

		for (int n = 2; n < numAmigos+1; n++) {

			int surpreendentes = numSimulacoes;

			// Simular cada jogo
			for (int i = 0; i < numSimulacoes; i++) {

				// Embaralhar a lista
				for (int ii = 0; ii < amigos.length - 1; ii++) {

					int r = ((int)(Math.random() * amigos.length));
					int t = amigos[ii];

					amigos[ii] = amigos[r];
					amigos[r] = t;
				}

				Boolean[] presenteados = new Boolean[amigos.length];

				for (int iii = 0; iii < presenteados.length; iii++) {
					presenteados[iii] = false;
				}

				// Checar se é surpreendente
				int jogador = amigos[0];

				for (int iiii = 0; iiii < amigos.length; iiii++) {

					presenteados[jogador] = true;
					jogador = amigos[iiii];

					if (iiii == amigos.length - 1) {
						if (presenteados[0]) {
							surpreendentes--;
						}
					}
				}
			}

			// Escrever o resultado
			System.out.println("p_" + n + " = " + (double)surpreendentes/numSimulacoes);
		}
	}
}