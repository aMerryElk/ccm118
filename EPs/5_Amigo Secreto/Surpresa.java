public class Surpresa {
	public static void main(String[] args) {
		
		int nMax =	Integer.parseInt(args[0]);
		int nGames =		Integer.parseInt(args[1]);

		// Test for all p_n until nMax
		for (int n = 2; n < nMax+1; n++) {

			int perfectGames = 0;

			// Construct list of participants
			int[] participants = new int[n];
				for (int i = 0; i < n; i++) participants[i] = i;

			//Iterate games
			for (int i = 0; i < nGames; i++) {
				if (isPerfectGame(shuffleIntArray(participants))) {
					perfectGames++;
				}
			}

			System.out.println("p_" + n + " = " + (double)perfectGames/nGames);
		}

	}

	//=================================================================
	// Function takes in an array of integers and shuffles it
	// Each number is switched places with a random number in the array
	public static int[] shuffleIntArray(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			int r = (int)(Math.random() * arr.length);
			int tmp = arr[i];
			arr[i] = arr[r];
			arr[r] = tmp;
		}
		return arr;
	}

	//=================================================================
	// Simulates the game with the permutation given and returns
	// true if it is a circular permutation
	public static Boolean isPerfectGame(int[] participants) {

		Boolean[] given = new Boolean[participants.length];
			for (int i = 0; i < given.length; i++) given[i] = false;

		int lastGifted = participants[0];

		for (int i = 0; i < participants.length; i++) {

			given[lastGifted] = true;
			lastGifted = participants[i];

			// If the last player to gift isnt gifting the first, then the game is not perfect
			if (i == participants.length - 1 && given[0]) return false;
		}
		return true;
	}

/*
	//Eu inicialmente escrevi essa magia negra pra checar se era uma
	//permutação perfeita, mas honestamente não sei como ela funciona,
	//então decidi reescrever. Mas funciona! Pode testar.
	//Eu adoraria saber como ela funciona =P
	//=================================================================
	public static Boolean isPerfectGame(int[] participants) {

		Boolean[] given = new Boolean[participants.length];
			for (int i = 0; i < given.length; i++) given[i] = false;

		for (int i = 0; i < participants.length; i++) {

			if (i == 0) { // Start the game
				given[participants[i]] = true;
			} else {
				if (given[participants[participants[i]]]) {
					return false;
				}
			}			
		}

		return true;
	}
*/
}