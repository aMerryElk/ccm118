public class GoodPerms {
	public static void main(String[] args) {
		
		int n =			Integer.parseInt(args[0]);
		int nPerms =	Integer.parseInt(args[1]);

		int goodPerms = 0;

		//Iterate permutations
		for (int i = 0; i < nPerms; i++) {

			// Construct list of participants
			int[] arr = new int[n];
				for (int j = 0; j < n; j++) arr[j] = j;

			permutateArray_noSelfReference(arr);

			for (int j = 0; j < arr.length; j++) System.out.print(arr[j] + " ");
			System.out.println();
		}

	}

	//=================================================================
	// Function takes in an array of integers and permutates it randomly
	// Avoids self-references (a[i] == i never happens)
	public static int[] permutateArray_noSelfReference(int[] arr) {
		for (int i = 1; i < arr.length; i++) {
			int r = (int)(Math.random() * (i));
			int tmp = arr[r]; arr[r] = arr[i]; arr[i] = tmp;
		}
		return arr;
	}
}