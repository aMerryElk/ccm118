public class ProbabilityGoodPerm {
	public static void main(String[] args) {
		
		int nMin =		Integer.parseInt(args[0]);
		int nMax =		Integer.parseInt(args[1]);
		int nGames =	Integer.parseInt(args[2]);

		// Test for all between nMin and nMax
		for (int n = nMin; n < nMax+1; n++) {

			int goodPerms = 0;

			// Construct list of participants
			// Done each time inside the loop because java
			int[] participants = new int[n];
				for (int i = 0; i < n; i++) participants[i] = i;

			//Iterate games
			for (int i = 0; i < nGames; i++) {

				if (isGoodPerm(permutateArray(participants))) {
					goodPerms++;
				}
			}

			System.out.println("q_" + n + " = " + (double)goodPerms/nGames);
		}

	}

	//=================================================================
	// Function takes in an array of integers and permutates it randomly
	public static int[] permutateArray(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			int r = (int)(Math.random() * (arr.length));
			int tmp = arr[r]; arr[r] = arr[i]; arr[i] = tmp;
		}
		return arr;
	}

	//=================================================================
	// Returns false if any self-references are found
	public static Boolean isGoodPerm(int[] a) {

		for (int i = 0; i < a.length; i++) {
			if (a[i] == i) return false;
		}

		return true;

	}
}