public class Test {

	public static void main(String[] args) {
		
		int sims = Integer.parseInt(args[0]);
		int N = Integer.parseInt(args[1]);

		for (int j = 0; j < sims; j++) {

			int[] a = new int[N];

			for (int i = 0; i < N; i++) a[i] = i;

			a = permutateArraySandra(a);

			for (int i = 0; i < a.length; i++) System.out.print(a[i] + ", ");
			System.out.print("::" + isPerfectGame(a));
			System.out.println();
		}
	}

//=================================================================
	// Simulates the game with the permutation given and returns
	// true if it is a circular permutation
	public static Boolean isPerfectGame(int[] a) {

		int i = 0;

		while (i >= 0) {
			int tmp = a[i];
			a[i] = -1;
			i = tmp;
		}

		for (i = 0; i < a.length; i++) if (a[i] != -1) return false;

		return true;
	}

//=================================================================
	// Makes an int array into a circular permutation
	// Sandra's algorithm
	public static int[] permutateArraySandra(int[] a) {
	
		int N = a.length;

		for (int i = 0; i < N - 1; i++) {
		  int r = i + 1 + (int) (Math.random() * (N - i - 1));
		  int t = a[r]; a[r] = a[i]; a[i] = t;
		}

		return a;
	}
}