public class Sandra {
	public static void main(String[] args) {
		
		int nMax =		Integer.parseInt(args[0]);
		int nGames =	Integer.parseInt(args[1]);

		// Test for all p_n until nMax
		for (int n = 2; n < nMax+1; n++) {

			int perfectGames = 0;

			// Construct list of participants
			int[] participants = new int[n];
				for (int i = 0; i < n; i++) participants[i] = i;

			//Iterate games
			for (int i = 0; i < nGames; i++) {

				if (isPerfectlyCiclic(permutateArray_Sandra(participants))) {
					perfectGames++;
				}
			}
			System.out.println("p_" + n + " = " + (double)perfectGames/nGames);
		}
	}

	//=================================================================
	// Function takes in an array of integers and permutates it
	// ---- Sandra's algorithm ----
	public static int[] permutateArray_Sandra(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			int r = 1 + i + (int)(Math.random() * (arr.length - i - 1));
			int tmp = arr[r]; arr[r] = arr[i]; arr[i] = tmp;
		}
		return arr;
	}

	//=================================================================
	// Simulates the game with the permutation given and returns
	// true if it is a circular permutation, false otherwise
	public static Boolean isPerfectlyCiclic(int[] a) {

		int i = 0;

		// Turns values into -1, following the order of the permutation
		// Stops when the next value is already -1
		while (i >= 0) {
			int tmp = a[i];
			a[i] = -1;
			i = tmp;
		}

		// If, at the end, any members are not -1, then the perm is not a complete cycle
		for (i = 0; i < a.length; i++) if (a[i] != -1) return false;

		return true;
	}
}