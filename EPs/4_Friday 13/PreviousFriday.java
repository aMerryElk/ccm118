public class PreviousFriday	{

	// Various redundant comments were omitted, check NextFriday.java for detailed comments

	public static void main(String[] args) {

		int year	= Integer.parseInt(args[2]);
		int month	= Integer.parseInt(args[1]);
		int day		= Integer.parseInt(args[0]);

		if (day <= 13) month--; // We're running backwards this time, so go to previous month if this 13th hasn't come yet
		day = 13;				// No need to test days that aren't 13ths

		int dow = dayOfWeek(year, month, day);
		while (dow != 5) {

			while (month > 0) {		// Backwards iteration

				dow = dayOfWeek(year, month, day);
				if (dow == 5) break;
				month--;			// Decrease the iterator
			}

			if (dow == 5) break;
			year--;
			month = 12;
		}

		System.out.println("Previous Friday 13th was " + day + "/" + month + "/" + year);
	}


	public static int dayOfWeek(int y, int m, int d) {

		int y0 = y - (14 - m) / 12;
		int x = y0 + y0/4 - y0/100 + y0/400;
		int m0 = m + 12 * ((14 - m) / 12) - 2;
		int d0 = (d + x + (31*m0)/12) % 7;

		return d0;
	}
}