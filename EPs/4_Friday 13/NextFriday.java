public class NextFriday	{

	public static void main(String[] args) {

		int year	= Integer.parseInt(args[2]);
		int month	= Integer.parseInt(args[1]);
		int day		= Integer.parseInt(args[0]);

		if (day >= 13) month++; // If 13 has already passed, skip to next month
		day = 13;				// No need to test days that aren't 13ths

		int dow = dayOfWeek(year, month, day);	// If we test a Fri-13th right off the bat, no need to proceed
		while (dow != 5) {	// If we test a Fri-13th right off the bat, no need to proceed

			while (month <= 12) {				// Loops through the months

				dow = dayOfWeek(year, month, day);	//What day is this months 13th?
				if (dow == 5) break;				// If it's a friday, stop the loop
				month++;							// If not stopped, increment the month counter
			}

			if (dow == 5) break;	// Stop the loop if we've reached a friday
			year++;					// If not stopped, increment the year counter
			month = 1;				// Reset the month counter to loop through a new year
		}

		System.out.println("Next Friday 13th is " + day + "/" + month + "/" + year);
	}


	public static int dayOfWeek(int y, int m, int d) {
		//Black magic taken from Creative Exercise 1.2.29, returns the day of the week for any given date

		int y0 = y - (14 - m) / 12;
		int x = y0 + y0/4 - y0/100 + y0/400;
		int m0 = m + 12 * ((14 - m) / 12) - 2;
		int d0 = (d + x + (31*m0)/12) % 7;

		return d0;
	}
}