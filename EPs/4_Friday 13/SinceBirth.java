public class SinceBirth	{

	// Various redundant comments were omitted, check NextFriday.java for detailed comments

	public static void main(String[] args) {

		int year	= Integer.parseInt(args[2]);
		int month	= Integer.parseInt(args[1]);
		int day		= Integer.parseInt(args[0]);

		int currYear = Integer.parseInt(args[3]);

		if (day >= 13) month++;
		day = 13;	// No need to test days that aren't 13ths

		int dow = dayOfWeek(year, month, day);
		int fridays = 0;			// Counts the fridays
		while (year < currYear) {	// As long as we havent reached the year we're counting towards, keep looping

			while (month <= 12) {

				dow = dayOfWeek(year, month, day);
				if (dow == 5) fridays++;	// If it's a friday, increment the count
				month++;					// Then check next month
			}

			year++;
			month = 1;
		}

		System.out.println("Number of Friday 13ths: " + fridays);
	}


	public static int dayOfWeek(int y, int m, int d) {

		int y0 = y - (14 - m) / 12;
		int x = y0 + y0/4 - y0/100 + y0/400;
		int m0 = m + 12 * ((14 - m) / 12) - 2;
		int d0 = (d + x + (31*m0)/12) % 7;

		return d0;
	}
}