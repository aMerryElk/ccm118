/******************************************************************************
 *
 * $ java DayOfWeekPedestrian 29 8 2018
 * 29/8/2018 is a We
 * $ java DayOfWeekPedestrian 2 8 1953 
 * 2/8/1953 is a Su
 * $ java DayOfWeekPedestrian 1 1 2000
 * 1/1/2000 is a Sa
 *
 ******************************************************************************/

public class DayOfWeekPedestrian {

    public static void main(String[] args) {
	int d = Integer.parseInt(args[0]);
	int m = Integer.parseInt(args[1]);
	int y = Integer.parseInt(args[2]);

	int[] DAYS = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	// days of the week: 0 .. 6: Sa .. Fr
	String[] WEEKDAYS = {"Sa", "Su", "Mo", "Tu", "We", "Th", "Fr"};
	// 1 1 1600 was a Saturday; I just know this (that is, Internet told me this)
	int dd = 1; int mm = 1; int yy = 1600; int day = 0;

	while (dd != d || mm != m || yy != y) {
	    day++; day %= 7;

	    boolean leapYear;
	    if (yy % 400 == 0) leapYear = true;
	    else if (yy % 100 == 0) leapYear = false;
	    else leapYear = yy % 4 == 0;

	    if (dd + 1 <= DAYS[mm] || dd == 28 && leapYear) dd++;
	    else if (mm < 12) { dd = 1; mm++; }
	    else { dd = 1; mm = 1; yy++; }
	}

	System.out.println(d + "/" + m + "/" + y + " is a " + WEEKDAYS[day]);
    }

}
