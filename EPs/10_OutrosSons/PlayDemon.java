public class PlayDemon{
	public static void main(String[] args) {

		String[] song = StdIn.readAllLines();

		for (int i = 0; i < song.length; i++) {
		//Each line is either a chord or an empty line

			System.out.println("Line " + (i+1) + "|  \t" + song[i]);
			//Just because it's oddly satisfying to see
			
			song[i] = song[i].trim();

			String[] chord = song[i].split("\\s+");
			//The last element is always the duration

			if (!song[i].equals("")) {
			//If the line isn't empty, compose it and play
				StdAudio.play(composeSoundFromChord(chord));				
			}
		}
	}


	//chord = {7, 6, 3, 8, ... , 1.5}
	public static double[] composeSoundFromChord(String[] chord) {

		//Each array is a note. Last element is left out because it's the duration
		double[][] sound = new double[chord.length - 1][];
		double duration = Double.parseDouble(chord[chord.length - 1]);

		//Synthesise each note individually first
		for (int note = 0; note < chord.length-1; note++) {

			int pitch = Integer.parseInt(chord[note]);
			double freq = 220.0 * Math.pow(2, pitch / 12.0);
			//I do prefer the lower octaves

			sound[note] = wave(freq, duration);
		}

		//Then average notes together to form a chord
		return avgDoubleArrays(sound);
	}


	//Averages arrays. All arrays must have the same length
	public static double[] avgDoubleArrays(double[][] arrays) {

		//How much weight each array is going to have in the result
		double weight = 1.0/arrays.length;

		double[] avgArray = new double[arrays[0].length];

		//For each array
		for (int i = 0; i < avgArray.length; i++) {

			//Sum its weighted elements into the final array
			for (int j = 0; j < arrays.length; j++) {
				avgArray[i] = avgArray[i] + (arrays[j][i]*weight);
			}

		}

		return avgArray;
	}

	//Simulates string-plucking timbre using Karplus-Strong
	public static double[] wave(double freq, double duration){

		int N = (int)(StdAudio.SAMPLE_RATE*duration);
		int p = (int)(StdAudio.SAMPLE_RATE/freq);

		double a[] = new double[N+1];

//		if (freq < 50) return a;
//		//Prevents a bug where it makes a spooky sound when dealing with low frequencies

		for (int i = 0; i < p; i++) a[i] = (Math.random() * 2) - 1;

		for (int i = p, j = 0; i <= N ; i++, j++)
			a[i] = .997*(a[j] + a[j+1])/2;

		return a;
	}




}
