public class SandraUniform {
	public static void main(String[] args) {

		int nMax	= Integer.parseInt(args[0]);
		int sims 	= Integer.parseInt(args[1]);
		String testPerm = args[2];
	
		int occurrences = 0;

		for (int i = 0; i < sims; i++) {

			// Creates a default permutation (0, 1, 2, ... N-1)
			int[] perm = new int[nMax];
				for (int j = 0; j < nMax; j++) perm[j] = j;

			permutateArray_Sandra(perm)	// Permutate using Sandra
			
			if (stringFromIntArray(perm).equals(testPerm)) {
				occurrences++;
			}
		}

		System.out.println("         # of occurrences of        test perm = " + occurrences);
		System.out.println("Expected # of occurrences of surprising perms = " + (double)sims/factorialOfInteger(nMax-1));
	}


//=================================================================
	// Function takes in an array of integers and permutates it
	// ---- Sandra's algorithm ----
	public static int[] permutateArray_Sandra(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			int r = 1 + i + (int)(Math.random() * (arr.length - i - 1));
			int tmp = arr[r]; arr[r] = arr[i]; arr[i] = tmp;
		}
		return arr;
	}

//=================================================================
	// Builds a string from an array of integers
	public static String stringFromIntArray(int[] arr) {
		String result = "";
		for (int i = 0; i < arr.length; i++) {
			result = result + arr[i];
		}
		return result;
	}

//=================================================================
	// Returns the factorial result of an integer
	public static int factorialOfInteger(int n) {
		int answer = n;
		for (int i = n-1; i > 1; i--) {
			answer = answer * i;
		}
		return answer;
	}

}