/*
 * $ java EqualStrings alpha alpha
 * Strings are equal
 * $ java EqualStrings alpha beta
 * Strings are different
 */

public class EqualStrings {

    public static void main(String[] args) {
	String a = args[0];
	String b = args[1];

	if (a.equals(b))
	    System.out.println("Strings are equal");
	else
	    System.out.println("Strings are different");
    }
    
}
